//Corbin Gomez
#ifndef PUZZLE_H
  #define PUZZLE_H

#include "stack.h"

/**********
Quadrants:

1x	2x	3x
4x	5x	6x
7x	8x	9x

***********/

class Puzzle
{
 public:
  Puzzle(const char grid[][9]) {}
  void solve(char grid[][9]);
}; // Puzzle class

bool column_has_num(char grid[][9], int column, char num);
bool row_has_num(char row[9], char num);
bool quadrant_has_num(char ** quadrant, char num);
bool get_next_star(int & row, int & column, char grid[][9]);
bool get_previous_star(int & row, int & column, char grid[][9]);
char int_to_char(int i);
char ** get_quadrant(char grid[][9], int row, int col);
char ** make_quadrant(char grid[][9], int quadrant_number);
void free_quadrant(char **quadrant);
int get_num_available_spots(char const grid[][9]);
bool is_between(int num, int left, int right);


#endif

