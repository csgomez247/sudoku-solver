Corbin Gomez

Sudoku Solver
-------------
Compiles with 'make'
Run executable with 'solve <puzzle>' where <puzzle> is one of the provided puzzle files or a user made file.

See puz2.txt, puz3.txt, or InkalaPuzzle.txt for formatting details.

InkalaPuzzle.txt is Finnish mathematician Arto Inkala's puzzle, considered the hardest puzzle in the world.
