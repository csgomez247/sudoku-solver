//Corbin Gomez
#include "puzzle.h"

using namespace std;

bool column_has_num(char grid[][9], int column, char num)
{
	for(int row = 0; row < 9; row++)
	{	  //if the column contains the number
		if(grid[row][column] == num)
			return true;
	}//for row

	return false;
}//column_has_num

bool row_has_num(char row[9], char num)
{
	for(int i = 0; i < 9; i++)
	{	  
		  //if the ith element of row equals num
		if(row[i] == num)
			return true;
	}//for index

	return false;
}//row_has_num

bool quadrant_has_num(char ** quadrant, char num)
{		
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			  //if the cell in the quadrant equals num
			if(quadrant[i][j] == num)
				return true;
		}//for j, column
	}//for i, row

	return false;
}//quadrant_has_num()

char ** get_quadrant(char grid[][9], int row, int col)
{
	char **quadrant = new char*[3];
	for(int i = 0; i < 3; i++)
		quadrant[i] = new char[3];

	  //if number is in quadrant 1
	if(is_between(row,0,2) && is_between(col,0,2))
		return make_quadrant(grid, 1);
	  //if number is in quadrant 2
	if(is_between(row,0,2) && is_between(col,3,5))
		return make_quadrant(grid, 2);
	  //if number is in quadrant 3
	if(is_between(row,0,2) && is_between(col,6,8))
		return make_quadrant(grid, 3);
	  //if number is in quadrant 4
	if(is_between(row,3,5) && is_between(col,0,2))
		return make_quadrant(grid, 4);
	  //if number is in quadrant 5
	if(is_between(row,3,5) && is_between(col,3,5))
		return make_quadrant(grid, 5);
	  //if number is in quadrant 6
	if(is_between(row,3,5) && is_between(col,6,8))
		return make_quadrant(grid, 6);
	  //if number is in quadrant 7
	if(is_between(row,6,8) && is_between(col,0,2))
		return make_quadrant(grid, 7);
	  //if number is in quadrant 8
	if(is_between(row,6,8) && is_between(col,3,5))
		return make_quadrant(grid, 8);
	  //if number is in quadrant 9
	if(is_between(row,6,8) && is_between(col,6,8))
		return make_quadrant(grid, 9);
	return quadrant;
}//get_quadrant

char ** make_quadrant(char grid[][9], int quadrant_number)
{
	char **quadrant = new char*[3];
	for(int i = 0; i < 3; i++)
		quadrant[i] = new char[3];

	switch(quadrant_number)
	{
		case(1):
		{
			for(int i = 0; i < 3; i++)
				for(int j = 0; j < 3; j++)
					quadrant[i][j] = grid[i][j];
			break;
		}//case 1

		case(2):
		{
			int k = 0;
			for(int i = 0; i < 3; i++)
			{
				int l = 0;
				for(int j = 3; j < 6; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 2

		case(3):
		{
			int k = 0;
			for(int i = 0; i < 3; i++)
			{
				int l = 0;
				for(int j = 6; j < 9; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 3

		case(4):
		{
			int k = 0;
			for(int i = 3; i < 6; i++)
			{
				int l = 0;
				for(int j = 0; j < 3; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 4

		case(5):
		{
			int k = 0;
			for(int i = 3; i < 6; i++)
			{
				int l = 0;
				for(int j = 3; j < 6; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 5

		case(6):
		{
			int k = 0;
			for(int i = 3; i < 6; i++)
			{
				int l = 0;
				for(int j = 6; j < 9; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 6

		case(7):
		{
			int k = 0;
			for(int i = 6; i < 9; i++)
			{
				int l = 0;
				for(int j = 0; j < 3; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 7

		case(8):
		{
			int k = 0;
			for(int i = 6; i < 9; i++)
			{
				int l = 0;
				for(int j = 3; j < 6; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 8

		case(9):
		{
			int k = 0;
			for(int i = 6; i < 9; i++)
			{
				int l = 0;
				for(int j = 6; j < 9; j++)
				{
					quadrant[k][l] = grid[i][j];
					l++;
				}//for j
				k++;
			}//for i
			break;
		}//case 9

		default: break;
	}//switch
	return quadrant;
}

void free_quadrant(char **quadrant)
{
	for(int i = 0; i < 3; i++)
		delete[] quadrant[i];
	delete[] quadrant;
}//free_quadrant

bool is_between(int num, int left, int right)
{
	  //if num is between left and right
	if(num >= left && num <= right)
		return true;
	return false;
}

int get_num_available_spots(char const grid[][9])
{
	int count = 0;
	for(int i = 0; i < 9; i++)
	{
		for(int j = 0; j < 9; j++)
		{
			  //if the cell has a *
			if(grid[i][j] == '*')
				count++;
		}//for
	}//for

	return count;
}//get_num_given_numbers


/*
	Passes in current row and column.
	Assumes that we know what is in grid[row][column]
*/
bool get_next_star(int & row, int & column, char grid[][9])
{
	  //if it is the last column
	if(column == 8)
	{
		row++;
		column = -1;
	}//if

	for(int j = column + 1; j < 9; j++)
	{
		  //if column j in row has a *
		if(grid[row][j] == '*')
		{
			column = j;
			return true;
		}//if

		  //if j is the index of the last column
		if(j == 8)
		{
			row++;
			j = -1;
		}//if
	}//for

	return false;
}//get_next_star

bool get_previous_star(int & row, int & column, char grid[][9])
{
	  //if function reaches left most column
	if(column == 0)
	{
		row--;
		column = 9;
	}//if

	for(int j = column -1; j >= 0; j--)
	{
		  //if cell (0,0) is a star
		if(row == 0 && j == 0 && grid[0][0] != '*')
			return false;

		  //if j is the left most column
		if(j == 0)
		{
			  //if column j of row has a star
			if(grid[row][j] == '*')
			{
				column = j;
				return true;
			}//if

			row--;
			j = 9;
		}//if

          //if column j of row is a star
		if(grid[row][j] == '*')
		{
			column = j;
			return true;
		}//if
	}//for

	return false;
}//get_previous_star

char int_to_char(int i)
{
	return (char)(((int)'0') + i);
}//int_to_char

void Puzzle::solve(char grid[][9])
{
	char original_grid[9][9];
	for(int i = 0; i < 9; i++)
		for(int j = 0; j < 9; j++)
			original_grid[i][j] = grid[i][j];

	Stack stack(get_num_available_spots(grid));
	int row = 0, column = -1; //has to be -1 for get_next_star to work properly
	int num = 1;
	bool testing_num = true;

	while(stack.is_full() == false)
	{
		get_next_star(row, column, original_grid);

			//if we are still testing the same number
		if(testing_num == false)
			testing_num = true;

		while(testing_num)
		{
			  //if the number works
			if(column_has_num(grid, column, int_to_char(num)) || 
			   row_has_num(grid[row], int_to_char(num))       ||
			   quadrant_has_num(get_quadrant(grid, row, column), int_to_char(num)))
			{
				bool incremented = false;
				  //if num exhausts all possibilities.
				while(num == 9)
				{
					grid[row][column] = '*';
					get_previous_star(row, column, original_grid);
					num = stack.get_top_num();

						//if the next number in stack is also a 9
					while(num == 9)
					{
						grid[row][column] = '*';
						get_previous_star(row, column, original_grid);
						stack.pop();
						num = stack.get_top_num();
					}//while

					num++;
					incremented = true;
					  //if the tested number is still 9
					if(num == 9)
					{
						stack.pop();
						break;
					}//if

					stack.pop();						
				}//if

				  //if num isn't 9 and hasn't been incremented previously in the loop
				if(num != 9 && incremented == false)
					num++;
			}//if

			else//the number does work
			{
				stack.push(num);
				grid[row][column] = int_to_char(num);
				num = 1;
				testing_num = false;
			}//else
		}//while
	}//while
}//solve
