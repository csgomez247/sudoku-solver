//Corbin Gomez
#ifndef STACK_H
	#define STACK_H

#include <iostream>

class Stack
{
 private:
 	int * stack;
 	int top;
 	int size;

 public:
 	Stack();
 	Stack(int num_available_spots);
 	~Stack();
 	void push(int num); 
 	void pop();
 	int get_size();
 	int get_top_index();
 	int get_top_num();
 	void print();
 	bool is_full();

};

#endif
