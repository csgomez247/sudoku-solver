//Corbin Gomez
#include "stack.h"

using namespace std;

Stack::Stack()
{
	stack = new int[81];
	top = -1;
	size = 81;
}//default Stack constructor

Stack::Stack(int num_available_spots)
{
	stack = new int[num_available_spots];
	top = -1;
	size = num_available_spots;
}//Stack constructor

Stack::~Stack()
{
	delete stack;
}//Stack destructor

void Stack::push(int num)
{
	top++;
	stack[top] = num;
}//push

void Stack::pop()
{
	top--;
}//pop

int Stack::get_size()
{
	return size;
}//size

int Stack::get_top_index()
{
	return top;
}//get_top_index

int Stack::get_top_num()
{
	if(top == -1)
		return -1;
	return stack[top];
}//get_top_num

void Stack::print()
{
	cout << "<-Bottom --- Top->\n";
	for(int i = 0; i <= top; i++)
		cout << stack[i] << " ";
	cout << endl;
}//print

bool Stack::is_full()
{
	if(top == size-1)
		return true;
	return false;
}//is_full
