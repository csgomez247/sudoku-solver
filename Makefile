#Corbin Gomez
CC=g++
CFLAGS=-Wall -ansi -g

TARGET=solve
HEADERS=puzzle.h stack.h
SOURCES=puzzle.cpp main.cpp stack.cpp

$(TARGET): $(SOURCES) $(HEADERS) 		
	$(CC) $(CFLAGS) -o $(TARGET) $(SOURCES)

clean:
	rm -f $(TARGET) 
